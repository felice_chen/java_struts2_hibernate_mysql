package service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import idao.ICommentFileDao;
import model.CommentFileModel;
import model.CommentModel;

public class CommentFileService {

	@Autowired
	ICommentFileDao commentFileDao;

	public List<CommentFileModel> findByCommentId(String commentId) throws SQLException {
		return commentFileDao.findByCommentId(commentId);
	}

	public void delByCommentFileId(String commentFileId) throws SQLException {
		commentFileDao.delByCommentFileId(commentFileId);
	}

	public List<CommentModel> findByCommentIds(List<CommentModel> cms) throws SQLException {

		List<String> commentIds = new ArrayList<String>();
		for (CommentModel cm : cms) {
			String commentId = cm.getCommentId();
			commentIds.add(commentId);
		}

		// 批次查詢 之後再分類成 map
		List<CommentFileModel> cfms = commentFileDao.findByCommentIds(commentIds);
		MultiValueMap<String, CommentFileModel> commentFilesMap = new LinkedMultiValueMap<String, CommentFileModel>();
		for(CommentFileModel cfm : cfms) {
			commentFilesMap.add(cfm.getCommentId(), cfm);
		}

		for (CommentModel cm : cms) {
			String commentId = cm.getCommentId();
			cfms = commentFilesMap.get(commentId);
			if (cfms != null) {
				cm.setCommentFiles(cfms);
			}
		}
		return cms;
	}

	public CommentFileModel findByCommentFileId(String commentFileId) throws Exception {
		return commentFileDao.findByCommentFileId(commentFileId);
	}

	public void downloadCommentFiles(HttpServletRequest request, HttpServletResponse response, CommentFileModel commentFileModel)
			throws Exception {

		String fileName = commentFileModel.getFileName();
		if (fileName == null || fileName.equals("")) {
			throw new ServletException("File Name can't be null or empty");
		}

		String path = commentFileModel.getUrl();
		File file = new File(path);
		if (!file.exists()) {
			throw new ServletException("File doesn't exists on server.");
		}
		System.out.println("File location on server::" + path);
		ServletContext ctx = request.getSession().getServletContext();
		InputStream fis = new FileInputStream(file);
		String mimeType = ctx.getMimeType(file.getAbsolutePath());
		response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
		response.setContentLength((int) file.length());
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

		ServletOutputStream os = response.getOutputStream();
		byte[] bufferData = new byte[1024];
		int read = 0;
		while ((read = fis.read(bufferData)) != -1) {
			os.write(bufferData, 0, read);
		}
		os.flush();
		os.close();
		fis.close();
		System.out.println("File downloaded at client successfully");
	}
}
