package model;

import lombok.Getter;  
import lombok.Setter;  

@Getter
@Setter
public class UserModel {
	
	String userId;

	String name;

	String email;
	
	String account;
	
	String password;

}
