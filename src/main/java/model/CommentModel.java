package model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentModel {

	String commentId;
	
	String content;
	
	String post_date;
	
	String userId;
	
	String userName;
	
	String articleId;
	
	String authotity;
	
	String status;
	
	String errorMessage;
	
	List<CommentFileModel> commentFiles = new ArrayList<CommentFileModel>();
}
