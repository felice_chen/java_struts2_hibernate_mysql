package dao;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import config.DBConfig;
import idao.IArticleDao;
import model.ArticleModel;
import model.DBModel;

public class ArticleDao implements IArticleDao {

	public ArticleModel createArticle(ArticleModel articleModel) throws SQLException {
		Date date = new Date();
		String nowDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);

		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		ArticleModel am = new ArticleModel();
		am.setTitle(articleModel.getTitle());
		am.setContent(articleModel.getContent());
		am.setPost_date(nowDateTime);
		am.setAuthority("public");
		am.setStatus("exist");
		am.setUserId(articleModel.getUserId());
		
		dbModel.getSession().save(am);
		dbConfig.close(dbModel);
		return am;
	}

}
