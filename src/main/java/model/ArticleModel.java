package model;

import lombok.Getter;  
import lombok.Setter;  

@Getter
@Setter
public class ArticleModel {
	
	String articleId;

	String title;

	String content;
	
	String post_date;
	
	String authority;
	
	String status;
	
	String userId;
	
	Integer commentCount;
}
