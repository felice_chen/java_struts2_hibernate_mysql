package service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import idao.ICommentDao;
import idao.IUserDao;
import model.CommentModel;
import model.UserModel;

public class CommentService {

	@Autowired
	UserService userService;

	@Autowired
	ICommentDao commentDao;
	
	@Autowired
	IUserDao userDao;

	public List<CommentModel> findAllByArticleId(String articleId) throws SQLException {

		List<CommentModel> cms = commentDao.findAllByArticleId(articleId);
		
		//TODO 有效能issue  要改成 join table or batch query
//		for (CommentModel cm : cms) {
//			cm.setUserName(userService.findNameById(cm.getUserId()));
//		}
		
		//暫時用這方式
		List<String> userIds = new ArrayList<String>();
		for(CommentModel cm : cms) {
			userIds.add(cm.getUserId());
		}
		
		List<UserModel> ums = userDao.findByUserIds(userIds);
		Map<String, String> nameByUserIdMap = new HashMap<String, String>();
		for(UserModel um : ums) {
			nameByUserIdMap.put(um.getUserId(), um.getName());
		}
		
		for(CommentModel cm : cms) {
			String username = nameByUserIdMap.get(cm.getUserId());
			cm.setUserName(username);
		}

		return cms;
	}

	public CommentModel createComment(CommentModel cm) throws SQLException {

		CommentModel newComment = commentDao.createComment(cm.getContent(), cm.getUserId(), cm.getArticleId());
		newComment.setUserName(userService.findNameById(newComment.getUserId()));
		newComment.setCommentFiles(cm.getCommentFiles());
		return commentDao.saveCommentFile(newComment);
	}

	public void delByCommentId(String commentId) throws SQLException {
		commentDao.delByCommentId(commentId);
	}

	public void updateByCommentIdAndContent(String commentId, String content) throws SQLException {
		commentDao.updateByCommentIdAndContent(commentId, content);
	}

}
