package action;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;

import lombok.Getter;
import lombok.Setter;
import model.CommentFileModel;
import service.CommentFileService;

@Getter
@Setter
public class DownloadAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	@Autowired
	CommentFileService commentFileService;

	private String fileId, fileName;

	@Override
	public String execute() throws Exception {
		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {

		CommentFileModel cfm = commentFileService.findByCommentFileId(fileId);
		fileName = cfm.getFileName();

		String path = ServletActionContext.getServletContext().getRealPath("/upload");
		String filepath = path + "/" + fileName;
		File file = new File(filepath);
		return FileUtils.openInputStream(file);
	}

	public String getDownloadFileName() {
		String downloadFileName = "";
		try {
			downloadFileName = URLEncoder.encode(fileName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return downloadFileName;
	}
}
