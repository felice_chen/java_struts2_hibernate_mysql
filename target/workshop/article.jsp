<jsp:include page="/template/template.jsp" />
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/js/article.js" />"></script>
<div id="wrapper">
	<div id="main" class="articleContainer">
		<article class="post">
			<header>
				<div class="title">
					<h2>廢文</h2>
					<span class="published article-post-time">2018-07-10
						15:25:20.0</span>
				</div>
			</header>
			<p>大家好</p>
			<footer>
				<ul class="actions">
					<!-- <li><a href="#" class="button big">Continue Reading</a></li> -->
				</ul>
			</footer>
		</article>
		<%-- <a href="<c:url value='/download/internal' />">Download This File (located inside project)</a> <br>
<a href="<c:url value='/commentFile/download/aaat/tx' />">123Download This File (located inside project)</a>  --%>
		<div style="font-size: 2.1em; color: black; display: inline;">
			回應
			<button id="commentBtn" class="btn btn-primary"
				onclick="openModal('commentModal')">我要留言</button>
		</div>


		<!-- commentModal HTML -->
		<div id="commentModal" class="modal fade ">
			<div class="modal-dialog modal-login comment-modal-size">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<button class="fa fa-plus btn-primary"
								style="color: white; border-radius: 5px; padding: 5px;"
								onclick="appendFileColumn()"></button>
							<span style="color: Red;">（ 檔案請小於 3MB ）</span>
							<!-- <form name="UploadForm" enctype="multipart/form-data"
								method="post" action="doupload.jsp"> -->
							<form id="aa" name="UploadForm" method="post">
								<div id="fileContainer">
									File 1 <input type="file" class="uploadFile" name="upload"
										size="50" maxlength="20" style="display: inline;" /> <br>
									File 2 <input type="file" class="uploadFile" name="upload"
										size="50" maxlength="20" style="display: inline;" />
								</div>
							</form>
						</div>
						<div class="form-group">
							<textarea id="commentContent" class="inputText comment-textArea"
								rows="4" cols="50"></textarea>
						</div>
						<div class="form-group">
							<input class="btn btn-primary btn-block btn-lg"
								onclick="submitComment()" value="OK !" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- delCommentModal HTML -->
		<div id="delCommentModal" class="modal fade ">
			<div class="modal-dialog modal-login" style="width: 400px; top: 30%;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" style="height: 100%;"
							data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body" style="text-align: center;">
						<div style="font-size: 20px; margin: 10px;">
							確定刪除嗎 ？<br>若有上傳檔案，也將一併刪除。
						</div>
						<div class="form-group">
							<button id="delCommentYesBtn" class="btn btn-primary btn-lg">Yes</button>
							<button class="btn btn-primary btn-lg" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- updateCommentModal HTML -->
		<div id="updateCommentModal" class="modal fade ">
			<div class="modal-dialog modal-login comment-modal-size">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<textarea id="updateCommentContent"
								class="inputText comment-textArea" rows="4" cols="50"></textarea>
						</div>
						<div class="form-group">
							<input id="updateCommentOkBtn"
								class="btn btn-primary btn-block btn-lg" value="OK!" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- fileCommentModal HTML -->
		<div id="fileCommentModal" class="modal fade ">
			<div class="modal-dialog modal-login comment-modal-size">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<!-- <form id="bb" name="DownloadForm" method="post" enctype="multipart/form-data"> -->

							<table id="commentFileTable">
								<tr>
									<th></th>
									<th style="width: 90%">檔案名稱（單擊檔名可下載檔案）</th>
								</tr>
							</table>
							<!-- </form> -->
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- delCommentFileModal HTML -->
		<div id="delCommentFileModal" class="modal fade ">
			<div class="modal-dialog modal-login" style="width: 400px; top: 30%;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" style="height: 100%;"
							data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body" style="text-align: center;">
						<div style="font-size: 20px; margin: 10px;">確定刪除嗎 ？</div>
						<div class="form-group">
							<button id="delCommentFileYesBtn" class="btn btn-primary btn-lg">Yes</button>
							<button class="btn btn-primary btn-lg" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
