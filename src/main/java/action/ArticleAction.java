package action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import lombok.Getter;
import lombok.Setter;
import model.ArticleModel;
import service.ArticleService;

@Getter
@Setter
public class ArticleAction extends ActionSupport implements ModelDriven<ArticleModel> {
	private static final long serialVersionUID = 1L;
	ArticleModel articleModel;

	@Autowired
	ArticleService acticleService;

	// TODO (此為額外功能-發文)改hibarnate後，日期和留言數量均為null，尚未處理。
	public void createArticle() throws Exception {
		acticleService.createArticle(articleModel);
	}

	public ArticleModel getModel() {
		if (articleModel == null) {
			return articleModel = new ArticleModel();
		}
		return articleModel;
	}

}
