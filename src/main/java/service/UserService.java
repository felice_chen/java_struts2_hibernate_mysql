package service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;

import idao.IUserDao;
import model.UserModel;

public class UserService {

	@Autowired
	IUserDao userDao;

	public String findIdByAccountAndPassword(String account, String password) throws Exception {
		return userDao.findIdByAccountAndPassword(account, password);
	}

	public void createUser(UserModel userModel) throws SQLException, Exception {

		UserModel isExistAccount = userDao.findByAccount(userModel.getAccount());

		if (isExistAccount != null) {
			throw new Exception("帳號已存在。");
		}

		userDao.createUser(userModel);
	}

	public String findNameById(String userId) throws SQLException {
		return userDao.findNameById(userId);
	}

	public String findNameByAccount(String account) throws SQLException {
		return userDao.findNameByAccount(account);
	}

	public String findIdByAccount(String account) throws SQLException {
		return userDao.findIdByAccount(account);
	}
}
