package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentFileModel {

	String commentFileId;
	
	String fileName;
	
	String url;
	
	String commentId;
}
