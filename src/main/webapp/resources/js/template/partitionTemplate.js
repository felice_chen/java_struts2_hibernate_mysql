function generateCommentTemplate(comment, fileIconDisplay, commonBtnDisplay) {
	return '<article class="post comment comment' + comment["commentId"]
			+ '" userId="' + comment["userId"] + '" commentId ="'
			+ comment["commentId"] + '">'
			+ '<span class="published comment-post-time">'
			+ comment["post_date"] + '</span><br>'
			+ '<span class="comment-username">' + comment["userName"]
			+ '</span>：' + '<span class="commentContent">' + comment["content"]
			+ '</span>' + '<button class="fa fa-paperclip comment-file-btn '
			+ fileIconDisplay + '" onclick=\"viewCommentFile(this)\"></button>'
			+ '<button class="fa fa-pencil circleIconBtn comment-update-btn '
			+ commonBtnDisplay + '" onclick=\"updateComment(this)\"></button>'
			+ '<button class="fa fa-trash circleIconBtn comment-del-btn '
			+ commonBtnDisplay + '" onclick=\"delComment(this)\"></button>'
			+ '</article>';
}

function generateFileInputColumnTemplate() {
	return '<input type="file" class="uploadFile" name="upload" size="50" maxlength="20" style="display: inline;" />';
}

function generateFileTableColumnTemplate(files, delFileIconDisplay) {
	return '<tr class="commentFileContent" commentFileId ="'
			+ files["commentFileId"]
			+ '" url="'
			+ files["url"]
			+ '">'
			+ '<td><button class="fa fa-trash '
			+ delFileIconDisplay
			+ '" style="border: none;" onclick="openConfirmCommentFileModal(this)"></button></td>'
			+ '<td>' + '<a href="doDownload.action?fileId='
			+ files["commentFileId"] + '">' + files["fileName"] + '</a></td>'
			+ '</tr>';
}

function generateArticleTemplate(article) {
	console.log(article);
	return '<article class="post">'
			+ '<header><div class="title">'
			+ '<h2><a href="#">' + article["title"] + '</a></h2>'
			+ '<span class="published article-post-time">'+article["post_date"]+'</span>'
			+ '</div></header>'
			+ article["content"]
			+ '<footer>'
			+ '<ul class="actions"></ul>'
			+ '<ul class="stats">'
			+ '<li><a href="#" class="icon fa-heart">28</a></li>'
			+ '<li><a href="#" class="icon fa-comment" onclick="viewPersonalBlogComment(' + article['articleId'] + ')">'
			+ article["commentCount"]
			+'</a></li>'
			+ '</ul>' + '</footer>'
			+ '</article>';
}
