var owner;
var account;

$(document).ready(function() {
	initPage();
	initPersonalBlog();
});

function initPage() {
	account = getSessionValueByKey("account");
	if (account) {
		loginHeadStatus();
	} else {
		logOutHeadStatus();
	}
}

function initPersonalBlog() {
	owner = $.url("?owner");
	if (owner) {
		$.ajax({
			url : "User/findNameByAccount",
			data : {
				account : owner
			}
		}).done(function(data) {
			$("#blogNameBlock").css("display", "");
			$("#blogName").text(data.name);
		}).fail(function(data, status, headers, config) {
			console.log("findNameById error !");
		});
	}
}

function openModal(modalId) {
	/* $(".modal-backdrop").remove(); */
	$(".modal").modal("hide");
	$(".modal .inputText").val("");
	$(".uploadFile").val("");
	$("#" + modalId).modal("show");
}

function login() {
	account = $("#account").val();
	$.ajax({
		url : "User/isExistUser",
		data : {
			account : account,
			password : $("#password").val()
		}
	}).done(
			function(data) {
				var account = data.account;
				var userId = data.userId;
				$("#loginModal").modal("hide");
				messageBox("successMessage", "登入成功!", 3000);
				$.session.set('account', account);
				$.session.set('userId', userId);
				loginHeadStatus();

				$("#commentBtn").css("display", "");

				var comments = $(".comment");
				for (var i = 0; i < comments.length; i++) {
					if ($(comments[i]).attr("userId") == userId) {
						$($(".comment-del-btn")[i]).removeClass(
								"comment-non-display");
						$($(".comment-update-btn")[i]).removeClass(
								"comment-non-display");
					}
				}
			}).fail(function(data, status, headers, config) {
		messageBox("errorMessage", "登入失敗!", 3000);
	});
}

function messageBox(messageId, messageText, millisecond) {
	$("#" + messageId + " strong").text(messageText);
	$("#" + messageId).css("display", "");
	setTimeout(function() {
		$("#" + messageId).hide();
	}, 3000);
}

function signup() {
	if ($("#newPasswordAgain").val() != $("#newPassword").val()) {
		messageBox("errorMessage", "密碼不一致！", 3000);
		return;
	}

	if (isNullAllInputText("registerModal")) {
		messageBox("errorMessage", "欄位皆為必填！", 3000);
		return;
	}

	$.ajax({
		url : "User/createUser",
		// url : "User/new",
		data : {
			name : $("#username").val(),
			email : $("#email").val(),
			account : $("#newAccount").val(),
			password : $("#newPassword").val(),
		}
	}).done(function() {
		$("#registerModal").modal("hide");
		messageBox("successMessage", "註冊成功", 3000);
	}).fail(function(data, status, headers, config) {
		messageBox("errorMessage", "帳號已被使用", 3000);
	});
}

function loginHeadStatus() {
	
	$("#userIcon").css("display", "");
	$("#logOutBtn").css("display", "");
	$("#loginBtn").css("display", "none");
	$("#registerBtn").css("display", "none");
	$("#postArticleBtn").css("display", "");

	var userId = getSessionValueByKey("userId");
	$.ajax({
		url : "User/findNameById",
		data : {
			userId : userId
		}
	}).done(function(data) {
		$("#userIcon span").text(data.name);
	}).fail(function(data, status, headers, config) {
		console.log("findNameById error !");
	});

}

function logOutHeadStatus() {
	$("#userIcon").css("display", "none");
	$("#logOutBtn").css("display", "none");
	$("#loginBtn").css("display", "");
	$("#registerBtn").css("display", "");
	$("#userIcon span").text("");
	$("#postArticleBtn").css("display", "none");
}

function isNullAllInputText(modalId) {
	var inputTexts = $("#" + modalId + " .inputText");
	var count = inputTexts.length;

	for (var i = 0; i < count; i++) {
		var inputText = $(inputTexts[i]);
		if (inputText.val() == "" || inputText.val() == null) {
			return true;
		}
	}

	return false;
}

function logOutYesBtn() {
	removeSessionByKey('account');
	removeSessionByKey('userId');
	$("#logOutModal").modal('hide');
	logOutHeadStatus();

	$("#commentBtn").css("display", "none");
	$(".comment .comment-del-btn").addClass("comment-non-display");
	$(".comment .comment-update-btn").addClass("comment-non-display")
}

function logOutNoBtn() {
	$("#logOutModal").modal('hide');
}

function getSessionValueByKey(key) {
	return $.session.get(key);
}

function removeSessionByKey(key) {
	$.session.remove(key);
}

function directPersonalBlog() {
	window.location.href = "./personalBlog.jsp?owner=" + account;
}