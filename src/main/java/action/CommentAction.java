package action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;

import lombok.Getter;
import lombok.Setter;
import model.CommentModel;
import service.CommentFileService;
import service.CommentService;

@Getter
@Setter
public class CommentAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private String articleId, commentId, content;
	List<CommentModel> commentModels;

	@Autowired
	CommentService commentService;

	@Autowired
	CommentFileService commentFileService;

	public void findByArticleId() throws Exception {
		commentModels = new ArrayList<CommentModel>();
		List<CommentModel> nonCommentFileDatasCMs = commentService.findAllByArticleId(articleId);
		
		List<CommentModel> completeData = new ArrayList<CommentModel>();
		if(nonCommentFileDatasCMs.size()!=0) {
			completeData = commentFileService.findByCommentIds(nonCommentFileDatasCMs);
		}
		commentModels.addAll(completeData);
	}

	public void updateByCommentIdAndContent() throws Exception {
		commentService.updateByCommentIdAndContent(commentId, content);
	}

	public void delByCommentId() throws Exception {
		commentService.delByCommentId(commentId);
	}
}
