package config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import model.DBModel;

public class DBConfig {
	Connection con = null;
	public Connection conDB() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection(
					"jdbc:mysql://127.0.0.1:3306/workshop?useUnicode=true&characterEncoding=UTF-8", "root", "0000");
		} catch (ClassNotFoundException e) {
			System.out.println("DriverClassNotFound :" + e.toString());
		} catch (SQLException x) {
			System.out.println("Exception :" + x.toString());
		}
		return con;
	}

	public Connection getCon() {
		return con;
	}

	public void close_(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	
	@SuppressWarnings("deprecation")
	public DBModel openSession() {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session session = sf.openSession();
		Transaction transaction = session.beginTransaction();
		return new DBModel(session, transaction);
	}
	
	public void close(DBModel dbModel) {
		dbModel.getTransaction().commit();
		dbModel.getSession().close();
	}
}
