package service;

import org.springframework.beans.factory.annotation.Autowired;

import idao.IArticleDao;
import model.ArticleModel;

public class ArticleService {

	@Autowired
	IArticleDao articleDao;
	
	public ArticleModel createArticle(ArticleModel articleModel) throws Exception  {
		
		ArticleModel am = articleDao.createArticle(articleModel);
		am.setCommentCount(0);
		return am;
	}

}
