package idao;

import java.sql.SQLException;
import java.util.List;

import model.CommentModel;

public interface ICommentDao {

	CommentModel createComment(String content, String userId, String articleId) throws SQLException;

	CommentModel saveCommentFile(CommentModel cm) throws SQLException;

	List<CommentModel> findAllByArticleId(String articleId) throws SQLException;

	void delByCommentId(String commentId) throws SQLException;

	void updateByCommentIdAndContent(String commentId, String content) throws SQLException;

}
