package idao;

import java.sql.SQLException;
import java.util.List;

import model.CommentFileModel;

public interface ICommentFileDao {
	
	List<CommentFileModel> findByCommentId(String commentId) throws SQLException;

	List<CommentFileModel> findByCommentIds(List<String> commentIds) throws SQLException;

	void delByCommentFileId(String commentFileId) throws SQLException;

	CommentFileModel findByCommentFileId(String commentFileId) throws SQLException;

}
