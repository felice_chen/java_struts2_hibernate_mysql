package dao;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import config.DBConfig;
import idao.ICommentDao;
import model.CommentFileModel;
import model.CommentModel;
import model.DBModel;

public class CommentDao implements ICommentDao {

	public CommentModel createComment(String content, String userId, String articleId) throws SQLException {
		Date date = new Date();
		String nowDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);

		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		CommentModel cm = new CommentModel();
		cm.setContent(content);
		cm.setPost_date(nowDateTime);
		cm.setAuthotity("public");
		cm.setStatus("exist");
		cm.setUserId(userId);
		cm.setArticleId(articleId);
		
		dbModel.getSession().save(cm);
		dbConfig.close(dbModel);
		return cm;
	}

	public CommentModel saveCommentFile(CommentModel cm) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		List<CommentFileModel> commentFiles = cm.getCommentFiles();
		for (CommentFileModel commentFile : commentFiles) {
			CommentFileModel cfm = new CommentFileModel();
			cfm.setFileName(commentFile.getFileName());
			cfm.setUrl(commentFile.getUrl());
			cfm.setCommentId(cm.getCommentId());
			dbModel.getSession().save(cfm);
			
			commentFile.setCommentFileId(cfm.getCommentFileId());
		}
		dbConfig.close(dbModel);
		return cm;
	}

	@SuppressWarnings("unchecked")
	public List<CommentModel> findAllByArticleId(String articleId) throws SQLException {

		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();


		Query query = dbModel.getSession()
				.createQuery("from CommentModel where article_id=:articleId Order By post_date");
		query.setString("articleId", articleId);
		List<CommentModel> cms = (List<CommentModel>) query.list();

		dbConfig.close(dbModel);
		return cms;
	}

	public void delByCommentId(String commentId) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		Query query = dbModel.getSession()
				.createQuery("delete CommentFileModel where comment_id=:commentId");
		query.setString("commentId", commentId);
		query.executeUpdate();
		
		query = dbModel.getSession()
				.createQuery("delete CommentModel where comment_id=:commentId");
		query.setString("commentId", commentId);
		query.executeUpdate();
		
		dbConfig.close(dbModel);
	}

	public void updateByCommentIdAndContent(String commentId, String content) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		Query query = dbModel.getSession()
				.createQuery("update CommentModel set content=:content where comment_id=:commentId");
		query.setString("content", content);
		query.setString("commentId", commentId);
		query.executeUpdate();
		
		dbConfig.close(dbModel);
	}
}
