package idao;

import java.sql.SQLException;

import model.ArticleModel;

public interface IArticleDao {

	ArticleModel createArticle(ArticleModel articleModel) throws SQLException;

}
