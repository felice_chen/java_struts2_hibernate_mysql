package action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import lombok.Getter;
import lombok.Setter;
import model.UserModel;
import service.UserService;

@Getter
@Setter
public class UserAction extends ActionSupport implements ModelDriven<UserModel> { // implements ModelDriven<UserModel>
	private static final long serialVersionUID = 1L;
	private String account, password, userId, name, email;

	UserModel userModel;

	@Autowired
	UserService userService;

	public void isExistUser() throws Exception {
		account = userModel.getAccount();
		password = userModel.getPassword();
		userId = userService.findIdByAccountAndPassword(account, password);
		userModel.setUserId(userId);
	}

	public void findNameById() throws Exception {
		userId = userModel.getUserId();
		name = userService.findNameById(userId);
		userModel.setName(name);
	}

	public void createUser() throws Exception {
		userService.createUser(userModel);
	}

	public void findNameByAccount() throws Exception {
		account = userModel.getAccount();
		name = userService.findNameByAccount(account);
		userModel.setName(name);
	}

	public UserModel getModel() {
		if (userModel == null) {
			return userModel = new UserModel();
		}
		return userModel;
	}

}
