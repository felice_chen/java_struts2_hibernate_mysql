package dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;

import config.DBConfig;
import idao.ICommentFileDao;
import model.CommentFileModel;
import model.DBModel;

public class CommentFileDao implements ICommentFileDao {
	
	@SuppressWarnings("unchecked")
	public List<CommentFileModel> findByCommentId(String commentId) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		Query query = dbModel.getSession()
				.createQuery("from CommentFileModel where comment_id=:commentId");
		query.setString("commentId", commentId);
		List<CommentFileModel> cfms = (List<CommentFileModel>) query.list();

		dbConfig.close(dbModel);
		return cfms;
	}

	@SuppressWarnings("unchecked")
	public List<CommentFileModel> findByCommentIds(List<String> commentIds) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		Query query = dbModel.getSession().createQuery("from CommentFileModel where comment_id in (:commentIds)");
		query.setParameterList("commentIds", commentIds);
		List<CommentFileModel> cfms = (List<CommentFileModel>)query.list();

		dbConfig.close(dbModel);
		return cfms;
	}
	
	public void delByCommentFileId(String commentFileId) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		Query query = dbModel.getSession()
				.createQuery("delete CommentFileModel where commentFile_id=:commentFileId");
		query.setString("commentFileId", commentFileId);
		query.executeUpdate();
		
		dbConfig.close(dbModel);
	}
	
	public CommentFileModel findByCommentFileId(String commentFileId) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		Query query = dbModel.getSession().createQuery("from CommentFileModel where commentFile_id=:commentFileId");
		query.setString("commentFileId", commentFileId);
		CommentFileModel cfm = (CommentFileModel) query.uniqueResult();

		dbConfig.close(dbModel);
		return cfm;
	}
}
