<!-- <html>
<body>
    <h2>Hello World</h2>
    <h3><a href="hello?name=Sally">Click here...</a></h3>
</body>
</html> -->

<!DOCTYPE HTML>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>workshop</title>
<meta charset="utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1" />
<link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/modal.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/myCss.css" />" rel="stylesheet">

<script src="<c:url value="/resources/js/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery/jquery.session.js" />"></script>
<script src="<c:url value="/resources/js/jquery/jquery-url-min.js" />"></script>

<script src="<c:url value="/resources/js/bootstrap/bootstrap.min.js" />"></script>

<script src="<c:url value="/resources/js/template/template.js" />"></script>
<script
	src="<c:url value="/resources/js/template/partitionTemplate.js" />"></script>

<script src="<c:url value="/resources/js/personalBlog.js" />"></script>
<%-- <script src="<c:url value="/resources/js/article.js" />"></script> --%>

</head>
<body>
	<!-- Wrapper -->

	<!-- Header -->

	<header id="header">
		<span id="blogNameBlock"
			style="margin: 0px 0px 0px 20px; display: none;"><span
			id="blogName"></span>'s Blog</span>
		<nav class="links">
			<ul>
				<li id="userIcon" style="display: none;">
					<button class="circleIconBtn" onclick="directPersonalBlog()">
						<i class="fa fa-user"></i>
					</button> <span></span>
				</li>
				<li id="postArticleBtn" onclick="openModal('newArticleModal')"
					style="display: none;"><a href="#">發文</a></li>
				<li id="logOutBtn" style="display: none;"
					onclick="openModal('logOutModal')"><a href="#">登出</a></li>
				<li id="loginBtn"><a href="#loginModal"
					onclick="openModal('loginModal')">登入</a></li>
				<li id="registerBtn"><a href="#registerModal"
					onclick="openModal('registerModal')">註冊</a></li>
			</ul>
		</nav>
	</header>

	<!-- <tiles:insert attribute="header" /> -->
	<!-- <div id="wrapper">

	</div> -->

	<!-- loginModal HTML -->
	<div id="loginModal" class="modal fade">
		<div class="modal-dialog modal-login" style="top: 20%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" style="height: 100%;"
						data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<i class="fa fa-user"></i><input id="account" type="text"
							class="form-control inputText" placeholder="Account"
							required="required">
					</div>
					<div class="form-group">
						<i class="fa fa-lock"></i> <input id="password" type="password"
							class="form-control inputText" placeholder="Password"
							required="required">
					</div>
					<div class="form-group">
						<input class="btn btn-primary btn-block btn-lg" onclick="login()"
							value="Login" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- registerModal HTML -->
	<div id="registerModal" class="modal fade">
		<div class="modal-dialog modal-login" style="top: 20%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" style="height: 100%;"
						data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<i class="fa fa-pencil"></i> <input id="username"
							class="form-control inputText" placeholder="username"
							required="required">
					</div>
					<div class="form-group">
						<i class="fa fa-envelope"></i> <input id="email"
							class="form-control inputText" placeholder="email"
							required="required">
					</div>
					<div class="form-group">
						<i class="fa fa-user"></i> <input id="newAccount" type="text"
							class="form-control inputText" placeholder="account"
							required="required">
					</div>
					<div class="form-group">
						<i class="fa fa-lock"></i> <input id="newPassword" type="password"
							class="form-control inputText" placeholder="password"
							required="required">
					</div>
					<div class="form-group">
						<i class="fa fa-lock"></i> <input id="newPasswordAgain"
							type="password" class="form-control inputText"
							placeholder="password again" required="required">
					</div>

					<div class="form-group">
						<input class="btn btn-primary btn-block btn-lg" onclick="signup()"
							value="sign up" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- logOutModal HTML -->
	<div id="logOutModal" class="modal fade">
		<div class="modal-dialog modal-login" style="top: 20%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" style="height: 100%;"
						data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body" style="text-align: center;">
					<div style="font-size: 20px; margin: 10px;">確定登出嗎 ？</div>
					<div class="form-group">
						<button class="btn btn-primary btn-lg" onclick="logOutYesBtn()">Yes</button>
						<button class="btn btn-primary btn-lg" onclick="logOutNoBtn()">No</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- newArticleModal HTML -->
	<div id="newArticleModal" class="modal fade ">
		<div class="modal-dialog modal-login comment-modal-size">
			<div class="modal-content">
				<div class="modal-header" style="padding: 0px; text-align: center;">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					發表新文章
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input id="newArticleTitle" type="text" class="inputText"
							placeholder="title" required="required">
					</div>
					<div class="form-group">
						<textarea id="newArticleContent"
							class="inputText comment-textArea" placeholder="content" rows="4"
							cols="50"></textarea>
					</div>
					<div class="form-group">
						<input id="newArticleOkBtn" onclick="postNewArticle()"
							class="btn btn-primary btn-block btn-lg" value="OK!" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="errorMessage" class="alert alert-danger alert-dismissible"
		style="width: 15%; letter-spacing: 0.25em; position: fixed; right: 20px; bottom: 20px; z-index: 9999; display: none;">
		<a href="#" class="close" onclick="$('#errorMessage').hide()"
			aria-label="close">&times;</a> <strong></strong>
	</div>

	<div id="successMessage" class="alert alert-success alert-dismissible"
		style="width: 15%; letter-spacing: 0.25em; position: fixed; right: 20px; bottom: 20px; z-index: 9999; display: none;">
		<a href="#" class="close" onclick="$('#successMessage').hide()"
			aria-label="close">&times;</a> <strong></strong>
	</div>

</body>
</html>
