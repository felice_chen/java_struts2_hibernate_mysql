package action;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;

import lombok.Getter;
import lombok.Setter;
import model.CommentFileModel;
import model.CommentModel;
import service.CommentService;

@Getter
@Setter
public class UploadAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private List<File> upload;
	private List<String> uploadFileName;
	private String errorMessage;
	private String userId, articleId, content;

	CommentModel commentModel;

	@Autowired
	CommentService commentService;

	public String execute() throws IOException, SQLException {

		CommentModel cm = new CommentModel();

		String path = ServletActionContext.getServletContext().getRealPath("/upload");
		File file = new File(path);
		if (!file.exists()) {
			file.mkdir();
		}
		
		List<CommentFileModel> cfms = new ArrayList<CommentFileModel>();
		if(upload != null) {
			for (int i = 0; i < upload.size(); i++) {
				CommentFileModel cfm = new CommentFileModel();
				String fileName = uploadFileName.get(i);
				
				File uploadFile = upload.get(i);
				long fileSize = uploadFile.length();
				
				if(fileSize > 1024 *1024 * 3) {
					errorMessage =  "檔案太大：" + fileName;
					return ERROR;
				}

				FileUtils.copyFile(uploadFile, new File(file, fileName));

				cfm.setFileName(fileName);
				cfm.setUrl(path + "/" + fileName);
				cfms.add(cfm);
			}
		}

		cm.setContent(content);
		cm.setArticleId(articleId);
		cm.setUserId(userId);
		cm.setCommentFiles(cfms);

		commentModel = commentService.createComment(cm);
		return SUCCESS;
	}

}
