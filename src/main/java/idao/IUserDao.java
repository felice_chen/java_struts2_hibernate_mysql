package idao;

import java.sql.SQLException;
import java.util.List;

import model.UserModel;

public interface IUserDao {

	String findIdByAccountAndPassword(String account, String password) throws Exception;

	UserModel findByAccount(String account) throws Exception;

	void createUser(UserModel userModel) throws SQLException;

	String findNameById(String userId) throws SQLException;

	String findNameByAccount(String account) throws SQLException;

	String findIdByAccount(String account) throws SQLException;
	
	List<UserModel> findByUserIds(List<String> userIds) throws SQLException;
}
