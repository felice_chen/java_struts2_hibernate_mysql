package model;

import org.hibernate.Session;
import org.hibernate.Transaction;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DBModel {

	Session session;
	Transaction transaction;
	
	public DBModel(Session session, Transaction transaction) {
		this.session = session;
		this.transaction = transaction;
	}
}
