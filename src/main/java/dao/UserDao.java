package dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;

import config.DBConfig;
import idao.IUserDao;
import model.DBModel;
import model.UserModel;

public class UserDao implements IUserDao {

	public String findIdByAccountAndPassword(String account, String password) throws Exception {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		Query query = dbModel.getSession().createQuery("from UserModel where account=:acc and password=:pwd");
		query.setString("acc", account);
		query.setString("pwd", password);
		UserModel usermodel = (UserModel) query.uniqueResult();

		dbConfig.close(dbModel);

		if (usermodel == null) {
			throw new Exception("帳戶或密碼錯誤。");
		}

		return usermodel.getUserId();
	}

	public UserModel findByAccount(String account) throws Exception {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		Query query = dbModel.getSession().createQuery("from UserModel where account=:acc");
		query.setString("acc", account);
		UserModel usermodel = (UserModel) query.uniqueResult();

		dbConfig.close(dbModel);

		if (usermodel != null) {
			throw new Exception("帳號已存在。");
		}
		
		return usermodel;
	}

	public void createUser(UserModel userModel) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		UserModel um = new UserModel();
		um.setName(userModel.getName());
		um.setEmail(userModel.getEmail());
		um.setAccount(userModel.getAccount());
		um.setPassword(userModel.getPassword());
		
		dbModel.getSession().save(um);
		dbConfig.close(dbModel);
	}

	public String findNameById(String userId) throws SQLException {
		
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		Query query = dbModel.getSession().createQuery("from UserModel where user_id=:userId");
		query.setString("userId", userId);
		UserModel usermodel = (UserModel) query.uniqueResult();

		dbConfig.close(dbModel);

		if (usermodel == null) {
			return null;
		}

		return usermodel.getName();
	}

	public String findNameByAccount(String account) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		Query query = dbModel.getSession().createQuery("from UserModel where account=:acc");
		query.setString("acc", account);
		UserModel usermodel = (UserModel) query.uniqueResult();

		dbConfig.close(dbModel);

		if (usermodel == null) {
			return null;
		}

		return usermodel.getName();
	}

	public String findIdByAccount(String account) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();

		Query query = dbModel.getSession().createQuery("from UserModel where account=:acc");
		query.setString("acc", account);
		UserModel usermodel = (UserModel) query.uniqueResult();

		dbConfig.close(dbModel);

		if (usermodel == null) {
			return null;
		}

		return usermodel.getUserId();
	}
	
	@SuppressWarnings("unchecked")
	public List<UserModel> findByUserIds(List<String> userIds) throws SQLException {
		
		DBConfig dbConfig = new DBConfig();
		DBModel dbModel = dbConfig.openSession();
		
		Query query = dbModel.getSession().createQuery("from UserModel where user_id in (:userIds)");
		query.setParameterList("userIds", userIds);
		List<UserModel> ums = (List<UserModel>)query.list();
		
		dbConfig.close(dbModel);
		return ums;
	}
}
