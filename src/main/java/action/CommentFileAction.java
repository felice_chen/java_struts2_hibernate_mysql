package action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;

import lombok.Getter;
import lombok.Setter;
import model.CommentFileModel;
import service.CommentFileService;
import service.CommentService;

@Getter
@Setter
public class CommentFileAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private String articleId, commentId, content, commentFileId;
	List<CommentFileModel> commentFileModels;

	@Autowired
	CommentService commentService;

	@Autowired
	CommentFileService commentFileService;

	public void findByCommentId() throws Exception {
		commentFileModels = new ArrayList<CommentFileModel>();
		List<CommentFileModel> cfms = commentFileService.findByCommentId(commentId);
		commentFileModels.addAll(cfms);
	}

	public void delByCommentFileId() throws Exception {
		commentFileService.delByCommentFileId(commentFileId);
	}

}
