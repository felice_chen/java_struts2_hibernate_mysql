function viewPersonalBlogComment(articleId) {
	var owner = $.url("?owner");
	window.location.href = "./article.jsp?owner=" + owner + "&articleId=" + articleId;
}

function postNewArticle(){
	console.log("postNewArticle");
	var userId = getSessionValueByKey("userId");
	
	if($("#newArticleTitle").val() == ""){
		messageBox("errorMessage", "文章標題不可為空!", 3000);
		return;
	}
	
	if($("#newArticleContent").val() == ""){
		messageBox("errorMessage", "文章內容不可為空!", 3000);
		return;
	}
	
	$.ajax({
		url : "Article/createArticle",
		data : {
			userId : userId,
			title : $("#newArticleTitle").val(),
			content: $("#newArticleContent").val()
		}
	}).done(function(data) {
		console.log(data);
		$("#newArticleModal").modal("hide");
		messageBox("successMessage", "發文成功!", 3000);
		$(".personalBlogContainer").append(generateArticleTemplate(data));
//		generateArticleTemplate();
//		$("#blogNameBlock").css("display", "");
//		$("#blogName").text(data.name);
	}).fail(function(data, status, headers, config) {
		console.log("create Article error !");
		messageBox("errorMessage", "發文失敗!", 3000);
	});
}